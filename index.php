<?php

include "BinaryTree.php";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Укр-Тех Інфо</title>
    <link rel="stylesheet" href="css/common.css">
</head>
<body>

<h1>Тестове</h1>
<?php
$deep = 3;
if (!empty($_POST['deep_tree'])) {
    $deep = (int)$_POST['deep_tree'];
}
?>

<div class="col-md-12">
    <div class="form_deep_tree">
        <form action="index.php" name="binary_tree" method="post">
            <input type="number" name="deep_tree" value="<?= $deep ?>" min="1" max="10" required autofocus>
            <input type="submit" value="Set Deep">
        </form>
    </div>
</div>

<?php
$tree = new BinaryTree();
$tree->runTree($deep);
$node = $tree->root;
$tree->showHtmlTree($node);
?>

</body>
</html>