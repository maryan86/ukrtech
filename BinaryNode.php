<?php

/**
 * Class BinaryNode
 */
class BinaryNode
{

    public $nameNode;
    public $depthNode;
    public $id;
    public $parentId;

    /**
     * BinaryNode constructor.
     * @param $item
     * @param int $id
     * @param int $parentId
     * @param int $depthNode
     * @param string $nameNode
     */
    public function __construct($item, $id = 0, $parentId = 0, $depthNode = 1, $nameNode = "root")
    {
        $this->id = $id + 1;
        $this->depthNode = $depthNode;
        $this->parentId = $parentId;
        $this->nameNode = $nameNode;
    }

    public function showNode()
    {
        $parent_id = "<div class='node-parent'>{$this->id}</div>";

        $pointer_left = "<div class='arrow-left'></div>";
        $info_block_center = "<div class='node-block'>{$parent_id}</div>";
        $pointer_right = "<div class='arrow-right'></div>";

        if ($this->nameNode != "ROOT")
            $node = "<div class='node'>{$pointer_left}{$info_block_center}{$pointer_right}</div>";
        else {
            $node = "<div class='node'>{$pointer_left}{$info_block_center}{$pointer_right}</div>";
        }
        echo $node;

    }

}