<?php

include "BinaryNode.php";

/**
 * Class BinaryTree
 */
class BinaryTree
{
    public $depthTree;
    public $root;

    /**
     * @param int $depthTree
     * @return BinaryNode
     */
    public function runTree($depthTree = 1)
    {

        $this->depthTree = $depthTree;
        
        $array = [];
        for ($i = 0; $i < 2 ** $depthTree - 1; $i++) {
            $array[$i] = rand(0, 100);
        }

        $node = new BinaryNode($array[0]);
        $root = $node;
        $queue = new SplQueue();
        $queue->enqueue($node);
        $queue->enqueue($node);
        $depthNode = $node->depthNode;

        for ($i = 1; $i < count($array); $i++) {
            $t = $queue->dequeue();
            $node = new BinaryNode($array[$i], $i);

            if ($depthNode <= 2 ^ ($t->depthNode - 1)) {
                $depthNode++;
                $node->depthNode = $t->depthNode + 1;
            } else {
                $depthNode++;
                $node->depthNode = $t->depthNode + 1;
            }

            if ($t->left == null) {
                $t->left = $node;
                $t->left->parentId = $t->id;
                if ($this->depthTree != $t->depthNode + 1) {
                    $t->left->nameNode = 'leftChild';
                } else {
                    $t->left->nameNode = 'left_list';
                }
            } else {
                $t->right = $node;
                $t->right->parentId = $t->id;
                if ($this->depthTree != $t->depthNode + 1) {
                    $t->right->nameNode = 'rightChild';
                } else {
                    $t->right->nameNode = 'right_list';
                }
            }

            $queue->enqueue($node);
            $queue->enqueue($node);
        }
        $this->root = $root;
        return $root;
    }

    /**
     * @param $node
     */
    public function showHtmlTree($node)
    {
        if ($node->parentId == 0) {
            echo "<ul class='root col-md-12'>";
            echo $node->showNode();
        } else {
            echo "<ul class='$node->nameNode'>";
            echo $node->showNode();
        }
        echo "<li>";

        if (isset($node->left->nameNode) || isset($node->right->nameNode)) {
            if ($node->left->nameNode == "leftChild" || $node->left->nameNode == "left_list") {
                $this->showHtmlTree($node->left);
            }
            if ($node->right->nameNode == "rightChild" || $node->right->nameNode == "right_list") {
                $this->showHtmlTree($node->right);
            }
        }
        echo "</li>";
        echo "</ul>";
    }
}